<?php

// creating a new instance of \DirSync
use DirSync\DirSync;

$dirSync = new DirSync();

// define a valid path of a json file
$filePath = "/DirSync/test.JSON";

// provide the instance with a JSON data file
$dirSync->fromFile($filePath);

// trigger the synchronization process
$dirSync->sync();

// print back the current directory tree
print_r(scandir(__DIR__));
