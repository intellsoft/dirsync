<?php

namespace DirSync;
use DirSync\Actions\CreateFolder;

/**
 * DirController
 *
 * @project dirSync
 * @date    2018-04-27
 * @author  Karel Petružela <petruzela@intellsoft.cz>
 */
class DirController {

    private $objectJSON;
    private $options;
    private $actions = [];

    public function __construct() {

    }

    public function setJSON($strJSON) {

        if ($strJSON==null)
            throw new Exceptions\InvalidInputException('Parameter strJSON is not a string. ['.$strJSON.']');

        $this->objectJSON = json_decode($strJSON);

    }

    public function setOptions($options) {

        $this->options = $options;
    }



    public function processSync() {

        $this->recursiveJSONRead($this->objectJSON);

        if (in_array(DirSync::SYNC_CREATE_ONLY,$this->options) || $this->options == null)
            new CreateFolder($this->actions[DirSync::SYNC_CREATE_ONLY]);

        if (in_array(DirSync::SYNC_ACTIONS_ONLY,$this->options) || $this->options == null)
            $this->customActions($this->actions[DirSync::SYNC_ACTIONS_ONLY]);

        if (in_array(DirSync::SYNC_REMOVE_ONLY,$this->options) || $this->options == null)
            new RemoveFolder($this->actions[DirSync::SYNC_REMOVE_ONLY]);

    }


    private function customActions($actions) {
        try {
            new $actions[0]($actions[1]);
        } catch (Exception $e) {
            throw new Exception("action ".$actions[0]." does not exist OR wrong parameters: ".$actions[1]);
        }
    }

    private function recursiveJSONRead($item, $path = "") {

        foreach ($item as $key => $value) {

            //object has subfolders or action inside
            if (is_object($value)) {

                //create the folder
                $path = $path."/".$key;

                //add path to the actions to create folder later
                $this->actions[DirSync::SYNC_CREATE_ONLY][$path];

                $this->{__FUNCTION__}($value, $path);

                //remove the last folder from the path as it goes level up
                str_replace("/".$key, "", $path);

                //is action or just folder null or false
            } else {

                if (substr($key, 0) === '#') {
                    $actionName = substr($key, 1);
                    //add new item to actions
                    $this->actions[DirSync::SYNC_ACTIONS_ONLY][$actionName][$value];
                }else
                    $this->actions[DirSync::SYNC_CREATE_ONLY][$path."/".$key];
            }
        }

        return "";
    }
} 