<?php

namespace DirSync\Actions;

/**
 * CreateFolder
 *
 * @project dirSync
 * @date    2018-04-27
 * @author  Karel Petružela <petruzela@intellsoft.cz>
 */
class CreateFolder extends AbstractFolderAction {

    public function doAction() {
        foreach ($this->folders as $folderPath){
            mkdir($folderPath);
        }
    }
}