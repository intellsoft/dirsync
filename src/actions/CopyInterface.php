<?php

interface CopyInterface {
    /**
     * @param mixed Indexed array where the index 0 is required and represent
     * the source path; index 1 is optional and represents the
     * destination path; When the index 1 is not provided
     * the default value is "./" which will represent the
     * directory in which the action has been triggered.
     * @param \DirSyncInterface $dirSync Reference to the DirSync instnace.
     */
    public function __constructor($parameters, $dirSync);
}