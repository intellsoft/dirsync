<?php
namespace DirSync\Actions;

interface FolderInterface {

    public function __constructor($folders);

    public function doAction();
}