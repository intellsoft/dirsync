<?php

namespace DirSync;


class DirSync implements DirSyncInterface {

    const SYNC_CREATE_ONLY = 'create_only';
    const SYNC_ACTIONS_ONLY = 'actions_only';
    const SYNC_REMOVE_ONLY = 'remove_only';

    private $JSON;

    public function __construct() {

    }

    public function setRootDir($path) {
        //set the Root dir from the JSON input

    }

    /**
     * @param string $filePath
     *
     * @return DirSyncInterface|void
     * @throws Exceptions\InvalidInputException
     */
    public function fromFile($filePath) {

        $this->validaFilePath($filePath);

        $this->dsadasdasdasdasdas($filePath, false);

        if(!$content = file_get_contents($filePath))
            throw new Exceptions\InvalidInputException('Error during file read. ['.$filePath.']');

        $this->setJsonInput($content);
    }

    public function setJsonInput($JSON) {

        if(!is_string($JSON))
            throw new Exceptions\InvalidInputException('Parameter JSON is not a string. ['.$JSON.']');

        $this->JSON = $JSON;

    }

    public function getJsonInput() {
        return $this->JSON;
    }

    public function sync($options = null) {

        $controller = new DirController();

        $controller->setJSON($this->getJsonInput(),$options);

        $controller->setOptions($options);

        $controller->processSync();

    }


    /**
     * @param $filePath
     *
     * @throws Exceptions\InvalidInputException
     */
    private function validaFilePath(& $filePath) {

        if(!is_file($filePath))
            throw new Exceptions\InvalidInputException('File not found. ['.$filePath.']');

        if(!is_readable($filePath))
            throw new Exceptions\InvalidInputException('File dsadasdsad. ['.$filePath.']');

    }

    private function dsadasdasdasdasdas($filePath, $false) {



    }

}

